-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 09/08/2013 às 13h19min
-- Versão do Servidor: 5.5.32
-- Versão do PHP: 5.4.15-1~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `concept_server`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `clipping`
--

CREATE TABLE IF NOT EXISTS `clipping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `midia_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Extraindo dados da tabela `clipping`
--

INSERT INTO `clipping` (`id`, `imagem`, `titulo`, `midia_id`) VALUES
(1, '1152189297_13e08144b7_z1.jpg', 'Teste', 0),
(2, '2719386997_d59889f8821.jpg', 'Teste', 0),
(3, '2719386997_d59889f8822.jpg', 'Teste', 0),
(4, '2719386997_d59889f8823.jpg', 'Teste', 0),
(5, 'galeria-lupa.png', 'Teste', 0),
(6, 'glyphicons-halflings.png', 'Teste', 0),
(16, 'dummy1.jpg', 'Teste', 4),
(17, '0d1357.jpg', 'Teste', 4),
(18, '0d13571.jpg', 'Teste', 4),
(19, '0d13572.jpg', 'Teste', 4),
(20, '0d13573.jpg', 'Teste', 4),
(21, '0d13574.jpg', 'Teste', 4),
(22, '0d13575.jpg', 'Teste', 4),
(23, '0d13576.jpg', 'Teste', 4),
(24, '0d13577.jpg', 'Teste', 4),
(25, 'dummy2.jpg', 'Teste', 5),
(26, '0d13578.jpg', 'Teste', 7),
(27, '0d13579.jpg', 'Teste', 8),
(28, 'dummy3.jpg', 'Teste', 8),
(29, '0d135710.jpg', 'Teste', 8),
(30, 'dummy4.jpg', 'Teste', 5),
(31, '0d135711.jpg', 'Teste', 5),
(32, '0d135712.jpg', 'Teste', 7),
(33, 'dummy5.jpg', 'Teste', 7),
(34, '0d135713.jpg', 'Teste', 4),
(35, '0d135714.jpg', 'Teste', 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos`
--

CREATE TABLE IF NOT EXISTS `contatos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `endereco` varchar(400) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `uf` varchar(2) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telefone` varchar(12) NOT NULL,
  `cep` varchar(12) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `contatos`
--

INSERT INTO `contatos` (`id`, `endereco`, `bairro`, `cidade`, `uf`, `email`, `telefone`, `cep`, `twitter`, `facebook`) VALUES
(1, 'Rua Marques de Olinda, 316, Casa 05', 'Ipiranga', 'São Paulo', 'SP', 'atendimento@concept.arq.br', '11 7828 5696', '04277-000', 'teste', 'http://facebook.com/teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `dicas`
--

CREATE TABLE IF NOT EXISTS `dicas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo_nav` varchar(255) DEFAULT NULL,
  `titulo_conteudo` varchar(255) DEFAULT NULL,
  `texto` text,
  `imagem` varchar(255) NOT NULL,
  `video` text NOT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `dicas`
--

INSERT INTO `dicas` (`id`, `titulo_nav`, `titulo_conteudo`, `texto`, `imagem`, `video`, `created`, `updated`) VALUES
(1, 'A Volta do Papel de Parede', 'Inovação das Paredes', '<p>Praticidade, bom gosto e um toque de sofistica&ccedil;&atilde;o. O Papel de Parede est&aacute; de volta ao mundo da decora&ccedil;&atilde;o e vem se tornando uma &oacute;tima op&ccedil;&atilde;o na hora de dar cor &agrave;s paredes do ambiente da casa. Ao contr&aacute;rio da tinta que obriga os moradores a dixarem suas residencias por conta do forte cheiro, o Papel de Parede tem aplica&ccedil;&atilde;o r&aacute;pida e exige menos mobiliza&ccedil;&atilde;o.</p>\n<p>No caso do Papel de Parede, n&atilde;o existe tantas precau&ccedil;&otilde;es a serem tomadas. Cerca de tr&ecirc;s a quatro horas s&atilde;o necess&aacute;rias para colocar o papel em um quarto com quatro paredes de oito metros cada. Por usar cola &agrave; base de &aacute;gua, pode-se dormir no ambiente no mesmo dia. Diferente da tinta, que costuma precisar de uma segunda dem&atilde;o.</p>\n<p>O retorno do uso do papel se deve ao fato de a cola usada hoje ser &agrave; base de &aacute;gua, sem afetar a parede. Al&eacute;m da qualidade do produto ter melhorado e ter resist&ecirc;ncia &agrave; umidade e ao vapor. As melhores marcas de papel duram em m&eacute;dia 10 anos. Em contrapartida, a tinta precisa de manuten&ccedil;&atilde;o de cinco em cinco anos.</p>\n<p>&nbsp;</p>', '', '<iframe width="420" height="315" src="http://www.youtube.com/embed/A-kMzxA-Ovw" frameborder="0" allowfullscreen></iframe>', 1363372108, 0),
(2, 'Escolha um Profissional Capacitado ', 'Para ter sucesso em sua obra', '<p>&Eacute; comum pensar que a fun&ccedil;&atilde;o do arquiteto resume-se apenas &agrave; quest&atilde;o est&eacute;tica. Grave erro, pois o Arquiteto &eacute; o grande idealizador e tamb&eacute;m um facilitador para que as coisas fluam melhor desde as primeiras id&eacute;ias at&eacute; a entrega das chaves, passando pelo planejamento da obra. <br /> Planejamento &eacute; condi&ccedil;&atilde;o b&aacute;sica para uma constru&ccedil;&atilde;o ou reforma ter o menor custo e ser &uacute;til aos seus futuros ocupantes. Como todo planejamento de obra come&ccedil;a pelo projeto.</p>\n<p>Ali&aacute;s, o profissional mais indicado para assumir a responsabilidade pela execu&ccedil;&atilde;o ou gerenciar a obra &eacute; o pr&oacute;prio autor dos projetos, pois conhece a fundo todas as suas particularidades. <br /><br /> Em &uacute;ltima an&aacute;lise seria, no m&iacute;nimo, uma grande imprud&ecirc;ncia e irresponsabilidade confiar o investimento de um razo&aacute;vel capital&nbsp;na elabora&ccedil;&atilde;o de um projeto e na execu&ccedil;&atilde;o da obra a pessoas sem experi&ecirc;ncia, qualifica&ccedil;&atilde;o e responsabilidade legal. <br /><br /> <span style="color: red;"><strong>Como identificar o bom profissional</strong></span> <br /><br /> Ao contratar os servi&ccedil;os de um arquiteto ou designer de interiores&nbsp;&eacute; importante ter a consci&ecirc;ncia de que, como em todas as profiss&otilde;es, existem os bons e os maus profissionais, portanto alguns aspectos devem ser observados para fazer uma boa escolha: <br /><br /> &bull; Cuidado ao analisar o profissional pelo valor mais baixo dos honor&aacute;rios, pois, certamente, eles ser&atilde;o proporcionais a sua experi&ecirc;ncia e compet&ecirc;ncia;</p>\n<p>&bull; Nunca contrate um servi&ccedil;o sem que seja firmado um contrato no qual dever&aacute; constar, claro e minuciosamente, tudo que se relacione ao seu prop&oacute;sito de forma a n&atilde;o ocasionar nenhuma d&uacute;vida futura principalmente quanto aos servi&ccedil;os que ser&atilde;o prestados, aos prazos, ao valor e &agrave;s condi&ccedil;&otilde;es de pagamento; <br /><br /> &bull; Mantenha um di&aacute;logo franco com o arquiteto ou designer de interiores. N&atilde;o omita nenhuma informa&ccedil;&atilde;o e fa&ccedil;a quest&atilde;o de participar diretamente do projeto e da obra. D&ecirc; sugest&otilde;es, expresse sua opini&atilde;o, &eacute; mais f&aacute;cil e mais barato fazer altera&ccedil;&otilde;es quando a obra ainda estiver em execu&ccedil;&atilde;o. Lembre-se de que o projeto deve atender as suas necessidades e n&atilde;o as do arquiteto ou designer de interiores. <br /><br /> Se, ainda assim, restar alguma d&uacute;vida quanto &agrave;s vantagens de contratar um arquiteto, compare o custo da contrata&ccedil;&atilde;o de um profissional competente em rela&ccedil;&atilde;o ao valor total da obra. Representa uma pequena fra&ccedil;&atilde;o, e os benef&iacute;cios de uma obra bem planejada compensam qualquer pagamento feito a um bom profissional.</p>', '', '', 1363178293, 0),
(3, 'Truques para Ampliar o Espaço', 'Espaços pequenos', '<p>&Eacute; preciso ter jogo de cintura na hora de montar uma decora&ccedil;&atilde;o confort&aacute;vel, aconchegante e organizada em um apartamento de poucos m&sup2;. Mas quebrar a cabe&ccedil;a para decorar um ambiente pequeno pode ser um desafio extremamente vantajoso com li&ccedil;&otilde;es para a vida, afinal de contas, quando temos pouco espa&ccedil;o precisamos priorizar as coisas, manter somente o essencial. Tem exerc&iacute;cio melhor do que esse para conhecermos um pouco mais de n&oacute;s mesmos?</p>\n<p>Ent&atilde;o a&iacute; vai a dica um: analise o que voc&ecirc; considera mais importante no dia a dia da sua casa. Isso vai definir se a sala de estar precisa de mais espa&ccedil;o para receber os amigos ou se um cantinho deve ser reservado para a vinda de uma crian&ccedil;a em um futuro pr&oacute;ximo. Coletando estas informa&ccedil;&otilde;es fica mais f&aacute;cil decidir como ficar&atilde;o os layouts do seu apartamento.D&ecirc; uma aten&ccedil;&atilde;o maior a circula&ccedil;&atilde;o do seu espa&ccedil;o, evitando ao m&aacute;ximo o ac&uacute;mulo de m&oacute;veis e outros itens. Quanto mais limpa sua casa for, maior ser&aacute; a sensa&ccedil;&atilde;o de amplitude. Deixe o ch&atilde;o &agrave; mostra optando por m&oacute;veis suspensos, ou com p&eacute;s que os levantem alguns cent&iacute;metros do ch&atilde;o. Isso j&aacute; vai dar uma baita diferen&ccedil;a no resultado final.</p>\n<p>Elimine paredes para ganhar ambientes mais espa&ccedil;osos, arejados e bonitos. Em um apartamento com poucas divis&oacute;rias, d&aacute; para brincar bastante com a versatilidade dos m&oacute;veis da sua casa: a sala de jantar pode virar home office e a sala de TV vira cantinho de leitura e espa&ccedil;o para receber visitas. Um pouco de parede a mostra tamb&eacute;m n&atilde;o faz mal a ningu&eacute;m. &Eacute; claro que voc&ecirc; pode (e deve!) decorar sua parede com quadros, mas n&atilde;o se esque&ccedil;a de deixar o todo balanceado. Uma estante vazada, por exemplo, pode ser uma &oacute;tima alternativa para dar um respiro ao ambiente.</p>\n<p>Espelhos d&atilde;o ilus&atilde;o de continuidade, use-os a seu favor prolongando os espa&ccedil;os e duplicando itens que merecem ganhar destaque. O branco &eacute; uma cor &oacute;tima para pequenos espa&ccedil;os, mas n&atilde;o somente ele. Qualquer tom pode funcionar muito bem no seu miniap&ecirc;, basta saber o jeitinho certo de us&aacute;-lo. Independentemente da cor, aposte no monocrom&aacute;tico para suavizar os limites do seu ambiente dando uma maior amplitude a composi&ccedil;&atilde;o. Fazendo dessa forma, n&atilde;o tem erro.</p>', '', '', 1362678872, 0),
(4, 'Decoração Corporativa', 'O Sucesso no visual da Empresa', '<p>Hoje em dia qualquer ambiente comercial ganha destaque por sua decora&ccedil;&atilde;o inovadora e cheia de vida, diferente do padr&atilde;o " sisuzo " de escrit&oacute;rios e demais com&eacute;rcios funcionais. O principal objetivo de uma decora&ccedil;&atilde;o comercial &eacute; porporcionar conforto aos seus clientes, fornecedores e funcion&aacute;rios, al&eacute;m de passar uma imagem de confian&ccedil;a e dinamismo.</p>\n<p>A escolha de m&oacute;veis, cores, texturas e demais itens, deve ser feita com cuidado e bom gosto, os propriet&aacute;rios devem ficar atentos com a combina&ccedil;&atilde;o dos elementos e n&atilde;o fugir da proposta da empresa ou das caracter&iacute;sticas que a marca j&aacute; representa no mercado.</p>\n<p>Os ambientes devem ser decorados n&atilde;o apenas para atender as necessidades de seus usu&aacute;rios, mas tamb&eacute;m para melhorar a qualidade de vida das pessoas que utilizam estes espa&ccedil;os. Usar revestimentos, quadros e objetos decorativos j&aacute; cria um efeito est&eacute;tico de destaque. A termatiza&ccedil;&atilde;o pode ser v&aacute;lida para todos os tipos de ambientes, desde que isso n&atilde;o interfira no bem-estar de seus colaboradores. &Eacute; necess&aacute;rio que todas as &aacute;reas sejam bem definidas e suas combina&ccedil;&otilde;es preservadas, para que o espa&ccedil;o n&atilde;o ganhe um tom sobrecarregado.</p>\n<p>Para quem ainda n&atilde;o tem certeza do tipo de decora&ccedil;&atilde;o que deseja empregar em seu escrit&oacute;rio, a dica &eacute; procurar ajuda de um profissional especializado.</p>', '', '', 1363782263, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `fotos`
--

CREATE TABLE IF NOT EXISTS `fotos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projeto_id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) NOT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=324 ;

--
-- Extraindo dados da tabela `fotos`
--

INSERT INTO `fotos` (`id`, `projeto_id`, `titulo`, `imagem`, `ordem`, `created`, `updated`) VALUES
(1, 0, 'Foto 0', 'foto_exemplo2.jpg', 3, 1362676238, 0),
(2, 0, 'Foto 1', 'foto_exemplo3.jpg', 7, 1362676238, 0),
(3, 0, 'Foto 2', 'foto_exemplo.jpg', 8, 1362676238, 0),
(4, 0, 'Foto 3', 'foto_exemplo.jpg', 4, 1362676238, 0),
(5, 0, 'Foto 4', 'foto_exemplo.jpg', 0, 1362676238, 0),
(6, 0, 'Foto 5', 'foto_exemplo.jpg', 5, 1362676238, 0),
(7, 0, 'Foto 6', 'foto_exemplo.jpg', 6, 1362676238, 0),
(8, 0, 'Foto 7', 'foto_exemplo4.jpg', 1, 1362676238, 0),
(31, 0, 'Teste', 'imagem-atuacao.jpg', 0, 1363280550, NULL),
(32, 0, 'Teste', '112.jpg', 0, 1363280606, NULL),
(33, 0, 'Teste', '113.jpg', 0, 1363280650, NULL),
(35, 0, 'Teste', '4.jpg', 0, 1363281524, NULL),
(36, 0, 'Teste', '41.jpg', 0, 1363281585, NULL),
(37, 0, 'Teste', '3.jpg', 0, 1363281829, NULL),
(62, 0, 'Teste', '48.jpg', 0, 1363289111, 0),
(77, 0, 'Teste', '640x9601.jpg', 2, 1363291729, 0),
(78, 0, 'Teste', '480x8004.jpg', 1, 1363360280, 0),
(79, 0, 'Teste', '1280x800.jpg', 0, 1363360378, 0),
(80, 0, 'Teste', '1920x1080.jpg', 0, 1363364415, NULL),
(88, 0, 'Teste', '1152189297_13e08144b7_z.jpg', 0, 1363703226, NULL),
(89, 0, 'Teste', '2719386997_d59889f882.jpg', 0, 1363703230, NULL),
(90, 0, 'Teste', '1152189297_13e08144b7_z1.jpg', 3, 1363703271, 0),
(91, 0, 'Teste', '2719386997_d59889f8821.jpg', 4, 1363703278, 0),
(92, 0, 'Teste', 'Modern_decoration_at_Larnaca_International_Airport_Republic_of_Cyprus.JPG', 1, 1363703281, 0),
(93, 0, 'Teste', '2719386997_d59889f8822.jpg', 1, 1363780250, 0),
(94, 0, 'Teste', '1152189297_13e08144b7_z2.jpg', 0, 1363780254, 0),
(95, 0, 'Teste', '2719386997_d59889f8823.jpg', 2, 1363782041, 0),
(96, 0, 'Teste', '1152189297_13e08144b7_z3.jpg', 0, 1363782127, 0),
(126, 17, 'Teste', 'cozinha_03.jpg', 14, 1364497231, 0),
(127, 17, 'Teste', 'cozinha_04.jpg', 15, 1364497252, 0),
(130, 17, 'Teste', 'cozinha_07.jpg', 16, 1364498265, 0),
(132, 17, 'Teste', 'cozinha_09.jpg', 17, 1364498292, 0),
(137, 17, 'Teste', 'hall_02.jpg', 21, 1364498353, 0),
(138, 17, 'Teste', 'hall_03.jpg', 0, 1364498360, 0),
(141, 17, 'Teste', 'lavabo_02.jpg', 23, 1364498389, 0),
(145, 17, 'Teste', 'sala_de_jantar_011.jpg', 1, 1364498423, 0),
(147, 17, 'Teste', 'sala_de_jantar_031.jpg', 4, 1364498439, 0),
(150, 17, 'Teste', 'sala_de_jantar_061.jpg', 5, 1364498466, 0),
(154, 17, 'Teste', 'sala_de_jantar_10.jpg', 2, 1364498505, 0),
(155, 17, 'Teste', 'sala_de_jantar_11.jpg', 7, 1364498509, 0),
(160, 17, 'Teste', 'sala_estar_02.jpg', 11, 1364498557, 0),
(162, 17, 'Teste', 'sala_estar_04.jpg', 13, 1364498574, 0),
(163, 17, 'Teste', 'sala_estar_04-2.jpg', 9, 1364498582, 0),
(164, 11, 'Teste', 'sala_de_jantar_012.jpg', 7, 1364499989, 0),
(165, 11, 'Teste', 'sala_de_jantar_022.jpg', 8, 1364500395, 0),
(170, 11, 'Teste', 'suíte_011.jpg', 0, 1364500465, 0),
(172, 11, 'Teste', 'suíte_022.jpg', 1, 1364500486, 0),
(174, 11, 'Teste', 'suíte_061.jpg', 2, 1364500528, 0),
(176, 11, 'Teste', 'varanda_011.jpg', 3, 1364500555, 0),
(177, 11, 'Teste', 'varanda_021.jpg', 4, 1364500564, 0),
(183, 11, 'Teste', 'varanda_081.jpg', 6, 1364500618, 0),
(185, 19, 'Teste', 'Cozinha_01.jpg', 23, 1364502103, 0),
(186, 19, 'Teste', 'Cozinha_02.jpg', 25, 1364502106, 0),
(192, 19, 'Teste', 'Cozinha_08.jpg', 34, 1364502126, 0),
(193, 19, 'Teste', 'Cozinha_09.jpg', 35, 1364502129, 0),
(194, 19, 'Teste', 'Cozinha_10.jpg', 31, 1364502133, 0),
(196, 19, 'Teste', 'Cozinha_12.jpg', 36, 1364502143, 0),
(197, 19, 'Teste', 'Cozinha_13.jpg', 33, 1364502145, 0),
(203, 19, 'Teste', 'Lavabo_04.jpg', 58, 1364502168, 0),
(213, 19, 'Teste', 'Sala-Estar-05.jpg', 8, 1364502199, 0),
(217, 19, 'Teste', 'Sala-Estar-09.jpg', 0, 1364502213, 0),
(218, 19, 'Teste', 'Sala-Estar-10.jpg', 32, 1364502215, 0),
(219, 19, 'Teste', 'Sala-Estar-11.jpg', 3, 1364502216, 0),
(220, 19, 'Teste', 'Sala-Estar-12.jpg', 6, 1364502221, 0),
(229, 19, 'Teste', 'Sala-Estar-21.jpg', 9, 1364502264, 0),
(230, 19, 'Teste', 'Sala-Estar-22.jpg', 11, 1364502269, 0),
(231, 19, 'Teste', 'Sala-Estar-23.jpg', 17, 1364502274, 0),
(232, 19, 'Teste', 'Sala-Estar-24.jpg', 16, 1364502279, 0),
(233, 19, 'Teste', 'Sala-Estar-25.jpg', 1, 1364502285, 0),
(234, 19, 'Teste', 'Sala-Estar-26.jpg', 40, 1364502290, 0),
(235, 19, 'Teste', 'Sala-Estar-27.jpg', 2, 1364502296, 0),
(236, 19, 'Teste', 'Sala-Jantar-01.jpg', 20, 1364502304, 0),
(257, 19, 'Teste', 'Suíte-master-01.jpg', 60, 1364502442, 0),
(259, 19, 'Teste', 'Suíte-master-03.jpg', 54, 1364502452, 0),
(263, 19, 'Teste', 'Suíte-master-07.jpg', 55, 1364502470, 0),
(266, 19, 'Teste', 'Suíte-master-10.jpg', 41, 1364502489, 0),
(270, 19, 'Teste', 'Suíte-master-14.jpg', 50, 1364502511, 0),
(272, 19, 'Teste', 'Suíte-master-16.jpg', 52, 1364502523, 0),
(273, 19, 'Teste', 'Suíte-master-17.jpg', 53, 1364502529, 0),
(274, 19, 'Teste', 'Suíte-master-18.jpg', 44, 1364502537, 0),
(276, 19, 'Teste', 'Suíte-master-20.jpg', 42, 1364502547, 0),
(277, 19, 'Teste', 'Suíte-master-21.jpg', 43, 1364502555, 0),
(286, 19, 'Teste', 'Varanda-01.jpg', 65, 1364502615, 0),
(287, 19, 'Teste', 'Varanda-02.jpg', 61, 1364502619, 0),
(288, 19, 'Teste', 'Varanda-03.jpg', 62, 1364502624, 0),
(292, 19, 'Teste', 'Varanda-07.jpg', 63, 1364502645, 0),
(299, 17, 'Teste', 'suíte_1_03.jpg', 22, 1365259672, 0),
(300, 17, 'Teste', 'suíte_1_18.jpg', 26, 1365259897, 0),
(301, 17, 'Teste', 'suíte_2_17.jpg', 27, 1365259986, 0),
(302, 17, 'Teste', 'varanda_032.jpg', 28, 1365260750, 0),
(303, 17, 'Teste', 'varanda_13.jpg', 29, 1365261008, 0),
(304, 20, 'Teste', 'DSC09926.JPG', 0, 1365428296, NULL),
(305, 20, 'Teste', 'DSC09897.JPG', 0, 1365428412, NULL),
(306, 20, 'Teste', 'DSC09901.JPG', 0, 1365428473, NULL),
(307, 20, 'Teste', 'DSC09944.JPG', 0, 1365428706, NULL),
(308, 21, 'Teste', 'Cozinha_061.jpg', 0, 1365430471, NULL),
(309, 22, 'Teste', 'Concept_(4).jpg', 1, 1365982886, 0),
(310, 22, 'Teste', 'Concept_(27).jpg', 0, 1366129983, 0),
(311, 22, 'Teste', 'Concept_(37).JPG', 2, 1366130067, 0),
(312, 22, 'Teste', 'Concept_(40).JPG', 3, 1366130339, 0),
(313, 23, 'Teste', 'DSC08677.JPG', 0, 1366131259, NULL),
(314, 23, 'Teste', 'DSC08684.JPG', 0, 1366131325, NULL),
(315, 23, 'Teste', 'DSC08801.JPG', 0, 1366131399, NULL),
(316, 23, 'Teste', 'DSC08815.JPG', 0, 1366131523, NULL),
(317, 21, 'Teste', 'Cozinha_041.jpg', 0, 1366133436, NULL),
(318, 21, 'Teste', 'Lavabo_031.jpg', 0, 1366133547, NULL),
(319, 21, 'Teste', 'Lavabo_051.jpg', 0, 1366133691, NULL),
(320, 21, 'Teste', 'Salas_14.jpg', 0, 1366133782, NULL),
(321, 21, 'Teste', 'Suite_1_08.jpg', 0, 1366138211, NULL),
(323, 21, 'Teste', 'Varanda_11.jpg', 0, 1366138387, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `midia`
--

CREATE TABLE IF NOT EXISTS `midia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) NOT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Extraindo dados da tabela `midia`
--

INSERT INTO `midia` (`id`, `titulo`, `imagem`, `ordem`, `created`, `updated`) VALUES
(4, 'Teste Mídia', 'imagem-atuacao.jpg', 0, 1363703745, 0),
(5, 'Teste Mídia 2', 'imagem-atuacao1.jpg', 0, 1363703755, 0),
(7, 'Teste', 'imagem-atuacao2.jpg', 0, 1363704128, 1363704128),
(8, 'Teste Cadastro', '2719386997_d59889f882.jpg', 0, 1363782206, 1363782206);

-- --------------------------------------------------------

--
-- Estrutura da tabela `paginas`
--

CREATE TABLE IF NOT EXISTS `paginas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `texto` text,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `template` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `paginas`
--

INSERT INTO `paginas` (`id`, `titulo`, `texto`, `imagem`, `ativo`, `created`, `updated`, `slug`, `description`, `template`) VALUES
(1, 'Pefil', '<p>A<strong> Concept &ndash; Arquitetura de Interiores</strong> desenvolve, desde 2010, projetos de interiores com a miss&atilde;o de transmitir a ess&ecirc;ncia de cada cliente em ambientes funcionais e agrad&aacute;veis, aliando valor est&eacute;tico, conforto, acessibilidade e a ergonomia.</p>\n<p><br />Com atendimento exclusivo o escrit&oacute;rio se destaca em criar espa&ccedil;os &uacute;nicos e integrados, com as particularidades de cada projeto e com o compromisso de oferecer solu&ccedil;&otilde;es criativas e ideias inovadoras, n&atilde;o importando a sua dimens&atilde;o.</p>\n<p><br />Al&eacute;m de projetos residenciais, a <strong>Concept &ndash; Arquitetura de Interiores</strong> tamb&eacute;m desenvolve solu&ccedil;&otilde;es para projetos coorporativos, com a garantia de realiza&ccedil;&atilde;o com tranquilidade e cumprimento de prazos e cronogramas.</p>\n<p><br />Contando com uma equipe de profissionais especializados em diversas &aacute;reas e que atuam em todas as etapas, o escrit&oacute;rio possui uma ampla rede de colaboradores e fornecedores que acompanham o processo de cria&ccedil;&atilde;o, desenvolvimento e implementa&ccedil;&atilde;o.</p>', 'imagem-perfil2.jpg', 1, 1362513475, 0, 'perfil', '<p>Perfil Concept Arquitetura</p>', ''),
(3, 'Atuação', '<p>A<strong> Concept - Arquitetura de Interiores</strong> trabalha com tr&ecirc;s grupos de servi&ccedil;os, que podem ser contratados separadamente ou de acordo com a necessidade de cada cliente.</p>\n<p>O trabalho pode ser desenvolvido para um projeto completo (planejamento, execu&ccedil;&atilde;o e acompanhamento da obra) ou atrav&eacute;s de uma consultoria espec&iacute;fica.</p>', 'imagem-atuacao4.jpg', 1, 1362659707, 0, 'atuacao', '', 'atuacao');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos`
--

CREATE TABLE IF NOT EXISTS `projetos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `texto` text,
  `capa` varchar(255) DEFAULT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Extraindo dados da tabela `projetos`
--

INSERT INTO `projetos` (`id`, `titulo`, `texto`, `capa`, `created`, `updated`, `ordem`) VALUES
(11, 'Vila Mariana', '<p>&nbsp; Criamos um espa&ccedil;o descontra&iacute;do, confort&aacute;vel e funcional para um empres&aacute;rio solteiro.</p>\n<p>&nbsp;&nbsp;</p>', 'varanda_02.jpg', 1364471472, 0, 6),
(17, 'Morumbi I', '<p>&nbsp; Um projeto alegre e confort&aacute;vel, com a sofistica&ccedil;&atilde;o de materiais naturais,&nbsp;apartamento com 97m&sup2;, no Jantar utilizamos Papel de Parede que d&aacute; destaque ao ambiente, no Hall de entrada uma mensagem energizante aos convidados, na sala de estar uma parede de pedras naturais.</p>', 'sala_de_jantar_14.jpg', 1364495666, 0, 2),
(19, 'Ipiranga ', '<p><span style="font-size: x-small;">&nbsp; O ponto de partida foi integrar a sala de estar, varanda e cozinha, a proposta &eacute; receber amigos e familiares, os ambientes ficaram muito aconchegantes e funcionais trazendo modernidade e luz ao projeto</span><span style="font-size: x-small;">.</span></p>', 'Cozinha_01.jpg', 1364500743, 0, 3),
(20, 'Saúde', '<p>Projeto executado para Jovem Casal apartamento de 55 m&sup2;, neste projeto com metragem reduzida levamos em conta a funcionalidade e dimens&otilde;es reduzidas para trazer conforto e praticidade ao dia a dia.</p>', 'DSC09901.JPG', 1365428189, 0, 4),
(21, 'Morumbi II', '<p>Apartamento de 97m&sup2;, a Ilumina&ccedil;&atilde;o foi privilegiada neste Projeto na cozinha uma sanca para deixar o ambiente suave e bem iluminada, os arm&aacute;rios foram projetados especialmente para as necessidades das moradoras,&nbsp; na sala o grande destaque &eacute; um grande painel que reveste a parede.</p>', 'Cozinha_011.jpg', 1365429147, 0, 5),
(22, 'Vila Olimpia ', '<p>Projeto Corporativo 95 m&sup2;, para este projeto levamos em conta as cores sobreas j&aacute; utilizadas pelo escritorio em suas outras unidades, os m&oacute;veis em cores pretas com peso e elegancia, os quadros remetendo a cidade de S&atilde;o Paulo nas d&eacute;cadas de 60, 70 e 80.</p>', 'DSC00439.JPG', 1365430983, 0, 1),
(23, 'Santana ', '<p>Vitrine Loja Elgin para Jovens Profissionais do Lar Center. Recriamos a atmosfera do Mercado Municipal na vitrine com os caixotes aparentes, gr&atilde;os, frutas e legumes que traduzem as cores e a regionalidade em nossa cidade.</p>', 'DSC08677.JPG', 1366130793, 0, 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) NOT NULL,
  `default` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `roles`
--

INSERT INTO `roles` (`id`, `role`, `default`) VALUES
(1, 'admin', 0),
(2, 'user', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE IF NOT EXISTS `servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` text,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id`, `titulo`, `descricao`, `created`, `updated`) VALUES
(9, 'Projeto de Interiores Completo', '<ul>\n<li>Briefing (necessidades e perfil do cliente)</li>\n<li>Detalhes da obra (demoli&ccedil;&otilde;es e constru&ccedil;&otilde;es)</li>\n<li>Proposta de projeto (layout)</li>\n<li>Detalhamento de marcenaria e mobili&aacute;rios</li>\n<li>Detalhamento de piso e revestimentos em geral</li>\n<li>Detalhamento de bancadas</li>\n<li>Projeto de ilumina&ccedil;&atilde;o</li>\n<li>Projeto de forro (molduras /sancas/ cortineiros)</li>\n<li>El&eacute;trica (existente e adicionais)</li>\n<li>Memorial descritivo</li>\n<li>Opcionais: paisagismo, projetos de ar condicionado, automa&ccedil;&atilde;o e ac&uacute;stica</li>\n<li>Consultoria em decora&ccedil;&atilde;o</li>\n</ul>', 1363870246, 0),
(10, 'Consultoria em decoração', '<ul>\n<li>Assessoria para defini&ccedil;&atilde;o de cores e quantitativos</li>\n<li>Escolha de tecidos, pisos, m&oacute;veis e adornos</li>\n</ul>', 1364222103, 1364222103),
(11, 'Acompanhamento de obra', '<ul>\n<li>Gest&atilde;o e cronograma de obra</li>\n<li>Gerenciamento or&ccedil;ament&aacute;rio e financeiro</li>\n<li>Coordena&ccedil;&atilde;o de fornecedores e prestadores de servi&ccedil;os</li>\n</ul>', 1364222132, 1364222132);

-- --------------------------------------------------------

--
-- Estrutura da tabela `slides`
--

CREATE TABLE IF NOT EXISTS `slides` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `padrao` tinyint(1) NOT NULL,
  `ordem` int(11) NOT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Extraindo dados da tabela `slides`
--

INSERT INTO `slides` (`id`, `titulo`, `link`, `imagem`, `padrao`, `ordem`, `created`, `updated`) VALUES
(6, 'Morumbi', 'http://concept.arq.br/previa/atuacao', 'home_slide2.jpg', 0, 2, 0, 0),
(12, 'Morumbi', 'http://concept.arq.br/previa/projetos/detalhe/17', 'sala_de_jantar_011.jpg', 0, 3, 0, 0),
(13, 'Ipiranga', 'http://concept.arq.br/previa/projetos/detalhe/19', 'Sala-Estar-26.jpg', 0, 4, 0, 0),
(14, 'Morumbi II', '', 'Varanda_05.jpg', 0, 1, 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `role_id` int(11) NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `role_id`, `activated`, `banned`, `ban_reason`, `new_password_key`, `new_password_requested`, `new_email`, `new_email_key`, `last_ip`, `last_login`, `created`, `modified`) VALUES
(1, 'trupe', '$P$Bm.afp74faAjfqPGOJPYzFREhWoN/S/', 'nilton@trupe.net', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '127.0.0.1', '2013-05-09 14:24:54', '2013-03-06 15:59:08', '2013-05-09 14:24:54'),
(2, 'concept', '$P$BhRioMVL4QVizUtGRFqO8vwbDzE6FE1', 'niltondefreitas@gmail.com', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '177.140.167.101', '2013-04-17 19:25:34', '2013-03-20 16:52:29', '2013-04-17 22:25:34');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_autologin`
--

CREATE TABLE IF NOT EXISTS `user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_profiles`
--

CREATE TABLE IF NOT EXISTS `user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `country`, `website`) VALUES
(1, 1, NULL, NULL),
(2, 2, NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
