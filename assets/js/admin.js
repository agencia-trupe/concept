function displayAlert(data){
    var type = 'alert-' + data.status;
    $('.top-alert').hide().addClass(type).fadeIn('fast', function(){
      $('.alert-content').text(data.texto);
    });
}

$(document.body).on('click', '.btn-reordenar', function(){
    $("#projeto-images").sortable({'disabled': false, 'delay':'100'});
    $("#projeto-images li").css({'cursor':'pointer'});
    $('.fotos-lista').addClass('reordena');
    $(this).removeClass('btn-info btn-reordenar')
           .addClass('btn-warning btn-salva-ordem')
           .text('Salvar ordem')
    return false;
  }).on('click', '.btn-salva-ordem', function(){
      $(this).removeClass('btn-warning btn-salva-ordem')
             .addClass('btn-info btn-reordenar')
             .text('Reordenar');
      $.ajax({
        type: "POST",
        url: location.protocol + "//" + location.hostname + "/painel/projetos/sort_fotos/",
        data: $("#projeto-images").sortable("serialize"),
        success: function(status){
          $("#projeto-images").sortable( "option", "disabled", true );
          $("#projeto-images li").css({'cursor':'default'});
          $(".fotos-lista").removeClass('reordena');
          var alerta = {'status':'success', 'texto':'Fotos reordenadas com sucesso.'}
          displayAlert(alerta);
        }
      });
      return false;
});
$(function(){
  /**
   * Fecha os alertas do topo.
   */
  $('.top-alert').on('click', '.close', function(){
    $('.top-alert').slideUp('fast', function(){
      $(this).removeClass('alert-success alert-error');
      $('.alert-content').text('');
    });
    return false;
  });
  /**
   * Exibe o formulário de adição de fotos;
   */
  $('.btn-adicionar-foto').click(function(){
    $('.form-adicionar-foto').slideDown().removeClass('invisible');
    return false;
  });
  $('.btn-adicionar-foto-cancela').click(function(){
    $('.form-adicionar-foto').slideUp();
    return false;
  });
  /**
   * Exclui uma foto relacionada a um projeto
   */
  function deletaFoto(target, module){
    var foto = target.parent();
    $.ajax({
        type: "POST",
        url: location.protocol + "//" + location.hostname + "/painel/" + module + "/deleta_foto/",
        data: {
          'ajax' : 1,
          'foto_id' : target.data('id')
        },
        success: function(msg){
          obj = JSON.parse(msg);
          var alerta = {'status':obj.status, 'texto':obj.msg }
          if(obj.status == "success"){
            $(foto).hide();
            displayAlert(alerta);
          } else {
            displayAlert(alerta);
          }
        }
    });
  }
  $(document.body).on('click', '.btn-delete', function(){
    deletaFoto($(this), $(this).data('module'));
    return false;
  });
  /*$(document.body).on('click', 'btn-delete', function(){
    
    return false;
  });*/
  function displayReordena()
  {
    if($('.btn-reordenar').hasClass('invisible'))
    {
      $('.btn-reordenar').removeClass('invisible');
    }
  }
  $('#projetos-upload').submit(function(e) {
      e.preventDefault();
      $.ajaxFileUpload({
         url         : location.protocol + "//" + location.hostname + "/painel/projetos/adiciona_foto/",
         secureuri      : false,
         fileElementId  : 'projeto-foto-upload',
         dataType    : 'json',
         data        : {
            'ajax' : 1,
            'projeto_id' : $('.id').val()
         },
         success  : function (data, status)
         {
            var alerta = {'status':data.status, 'texto' : data.msg}
            if(data.status == 'error')
            {
              displayAlert(alerta);
            }
            else
            {
              var foto_template = ich.foto_template(data);
              displayReordena();
              $('#projeto-images').append(foto_template);
              displayAlert(alerta);
            }
         }
      });
      return false;
   });

  $('#clipping-upload').submit(function(e) {
      e.preventDefault();
      $.ajaxFileUpload({
         url         : location.protocol + "//" + location.hostname + "/midia/adiciona_foto/",
         secureuri      : false,
         fileElementId  : 'projeto-foto-upload',
         dataType    : 'json',
         data        : {
            'ajax' : 1,
            'midia_id' : $('.id').val()
         },
         success  : function (data, status)
         {
            var alerta = {'status':data.status, 'texto' : data.msg}
            if(data.status == 'error')
            {
              displayAlert(alerta);
            }
            else
            {
              var midia_template = ich.midia_template(data);
              displayReordena();
              $('#midia-images').append(midia_template);
              displayAlert(alerta);
            }
         }
      });
      return false;
   });
});