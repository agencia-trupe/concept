<?php
class Servicos extends MX_Controller
{
    var $data;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('servicos/servico');
    }

    public function parcial()
    {
        $this->data['servicos'] = $this->servico->get_all();
        $this->load->view('servicos/parcial', $this->data);
    }

    public function insert()
    {
        $servico = new Servico();
        $servico->titulo = 'Serviço teste 3';
        $servico->descricao  = '<ul>';
        $servico->descricao .= '<li>Conteúdo 1</li>';
        $servico->descricao .= '<li>Conteúdo 2</li>';
        $servico->descricao .= '<li>Conteúdo 3</li>';
        $servico->descricao .= '</ul>';
        $servico->created    = time();
        if($servico->save())
        {
            echo 'Ok!';
        }
        else
        {
            echo 'Nooooooo!';
        }
    }
}