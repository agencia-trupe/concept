    <ul class="bxslider">
        <?php foreach($slides as $slide): ?>
        <li data-id="<?=$slide->id; ?>">
        	<?php if ($slide->link): ?>
        		<a href="<?=$slide->link; ?>" class="slide-content">
					<?=$slide->titulo; ?>
				</a>
        	<?php endif ?>
        </li>
      <?php endforeach; ?>
    </ul>
<div class="slide_nav">
    <div class="control" id="slide_left"></div>
    <div class="control" id="slide_right"></div>
</div>
<div class="clearfix"></div>