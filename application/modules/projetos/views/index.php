<div class="conteudo projetos">
    <?php if ( isset($projetos) ):
          foreach ( $projetos as $projeto ): ?>
        <a href="<?=site_url( 'projetos/detalhe/' . $projeto->id ); ?>" class="projeto-box">
            <img src="<?=base_url( 'assets/img/projetos/capas/' . $projeto->capa ); ?>" 
             alt="<?=$projeto->titulo; ?>">
            <span class="titulo"><?=$projeto->titulo; ?></span>
            <div class="lupa"></div>
        </a>
    <?php endforeach; 
        endif; ?>
    <div class="clearfix"></div>
</div>