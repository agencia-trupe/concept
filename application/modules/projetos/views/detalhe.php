<div class="conteudo projetos galeria">
    <div class="projeto-sidebar">
        <div class="projeto-meta">
            <div class="clearfix"></div>
           <h1><?=$projeto->titulo; ?></h1>
           <div class="separador"></div>
           <div class="projeto-descricao"><?=$projeto->texto; ?></div>
        </div>
        <div class="projeto-navegacao">
            <div class="nav-container">
                <?php if($prev): ?>
                <?=anchor( 'projetos/detalhe/' . $prev, '<span>anterior</span>', 'class="projeto-anterior"'); ?>
                <?php endif; ?>
            </div>
            <div class="grid-container">
                <a href="<?=site_url( 'projetos' ); ?>" class="projeto-grid"></a>
            </div>
            
            <div class="nav-container next">
                <?php if($next): ?>
                <?=anchor( 'projetos/detalhe/' . $next, '<span>próximo</span>', 'class="projeto-proximo"'); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="projeto-galeria">
        <?php foreach ( $fotos as $foto ): ?>
            <a rel="prettyPhoto[projeto_<?=$projeto->id; ?>]" href="<?=base_url('assets/img/projetos/fotos/' . $foto->imagem ); ?>" class="projeto-foto">
                <img src="<?=base_url('assets/img/projetos/fotos/thumbs/' . $foto->imagem ); ?>" alt="<?=$foto->titulo; ?>">
                <div class="projeto-galeria-lupa"></div>
            </a>
        <?php endforeach; ?>
    </div>
</div>