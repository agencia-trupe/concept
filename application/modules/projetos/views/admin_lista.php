<div class="row-fluid">
    <div class="span9">
        <legend>Projetos <a class="btn btn-info btn-mini" href="<?=site_url('painel/projetos/cadastrar'); ?>">Novo</a></legend>
     <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="span1">Ordem</th><th class="span6">Título</th><th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($projetos as $projeto): ?>
                <tr>
                    <td><?=$projeto->ordem; ?></td>
                    <td><?=$projeto->titulo; ?></td>
                    <td><?=anchor('projetos/admin_projetos/editar/' . $projeto->id, 'Editar', 'class="btn btn-mini btn-warning"'); ?>
                        <?=anchor('projetos/admin_projetos/deleta_projeto/' . $projeto->id, 'Remover', 'class="btn btn-mini btn-danger"'); ?></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
</div>