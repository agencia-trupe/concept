<?php
class Projetos extends MX_Controller
{
    var $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('projetos/projeto');
        $this->load->model('projetos/foto');
        $this->data['pagina'] = 'projetos';
    }
    public function index()
    {
        $this->lista();
    }
    public function lista()
    {
        $this->data['projetos'] = $this->projeto->get_all();
        $this->data['conteudo'] = 'projetos/index';
        $seo = array(
            'titulo' => 'Projetos',
            'descricao' =>  'Conceça os projetos realizados pela Concept
                            arquitetura de interiores' 
            );
        $this->load->library( 'seo', $seo );
        $this->load->view( 'layout/template', $this->data );
    }

    public function detalhe( $id )
    {
        $this->load->model( 'projetos/foto' );
        //busca os dados de uma página cujo slug foi passado como parâmetro
        $projeto = $this->projeto->get_conteudo( $id );
        if( !is_null( $projeto ) )
        {
            $this->data['fotos'] = $this->foto->get_projeto($projeto->id);
            //Variável com os dados da página a ser enviada para a view
            $this->data['projeto'] = $projeto;
            //Variáveis com os ids de posts anterior e próximo.
            $this->data['prev'] = $this->projeto->get_related( $projeto->ordem, 'prev' );
            $this->data['next'] = $this->projeto->get_related( $projeto->ordem, 'next' );
            //Carrega a biblioteca de SEO e a inicializa.
            $seo = array(
                'title' => $projeto->titulo,
                'description' => $projeto->descricao,
                );
            $this->load->library( 'seo', $seo );
            //Define a view utilizada
            $this->data['conteudo'] = 'projetos/detalhe';
            //Carrega a view especificada como parâmetro e exibe a página
            $this->load->view( 'layout/template', $this->data );
        }
        else
        {
            show_404();
        }
    }

    private function _insert()
    {
        for ($i=0; $i < 9; $i++) { 
            $dados = array();
            $dados['titulo'] = 'Projeto ' . $i;
            $dados['data'] = 2010;
            $dados['texto'] = 'Descrição do projeto ' . $i;
            $dados['capa'] = 'projeto_' . $i . '.jpg';
            $insert = $this->projeto->insert($dados);
            if($insert)
            {
                echo 'Ok!';
            }
            else
            {
                echo 'Nooooo!';
            }
        }
    }

    public function insert_foto($projeto_id)
    {
        for ($i=0; $i < 30; $i++) { 
            $dados = array();
            $dados['titulo'] = 'Foto ' . $i;
            $dados['imagem'] = 'foto_exemplo.jpg';
            $dados['projeto_id'] = $projeto_id;
            $insert = $this->foto->insert($dados);
            if($insert)
            {
                echo 'Ok!';
            }
            else
            {
                echo 'Nooooo!';
            }
        }
    }
}