<?php
class Dicas extends MX_Controller
{
    var $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('dicas/dica');
        $this->data['pagina'] = 'dicas';
    }
    public function index()
    {
        $this->lista();
    }
    public function lista()
    {
        $this->data['dicas'] = $this->dica->get_all();
        $this->data['conteudo'] = 'dicas/index';
        $seo = array(
            'titulo' => 'Dicas',
            'descricao' =>  'Dicas da Concept Arquitetura de Interiores' 
            );
        $this->load->library( 'seo', $seo );
        $this->load->view( 'layout/template', $this->data );
    }
}