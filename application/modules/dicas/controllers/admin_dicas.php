<?php
class Admin_dicas extends MX_Controller
{
    var $data;
    public function __construct()
    {
        parent::__construct();
        $this->data['module'] = 'dicas';
        $this->load->model('dicas/dica');
    }
    public function index()
    {
        $this->lista();
    }

    public function lista()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Concept - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['dicas'] = $this->dica->get_all();
            $this->data['conteudo'] = 'dicas/admin_lista';
            $this->load->view('start/template', $this->data);
        }
    }

    public function editar($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Concept - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['acao'] = 'editar';
            $this->data['dica'] = $this->dica->get_conteudo($id, 'id');
            $this->data['conteudo'] = 'dicas/admin_edita';
            $this->load->view('start/template', $this->data);
        }
    }

    public function cadastrar()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Concept - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['acao'] = 'cadastrar';
            $this->data['conteudo'] = 'dicas/admin_edita';
            $this->load->view('start/template', $this->data);
        }
    }

    public function processa()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Concept - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('dicas'))
            {
                $this->data['acao'] = 'editar';
                $this->data['dica'] = $this->dica
                ->get_conteudo($this->input->post('id'));
                $this->data['conteudo'] = 'dicas/admin_edita';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }

                if(strlen($_FILES["imagem"]["name"])>0)
                {
                    
                    $config['upload_path'] = './assets/img/dicas/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '8000';
                    $config['max_width']  = '4000';
                    $config['max_height']  = '4000';

                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload('imagem'))
                    {
                            $this->data['error'] = array('error' => $this->upload->display_errors());
                            $this->data['acao'] = 'editar';
                            $this->data['pagina'] = $this->pagina->get_conteudo($id, $this->input->post('id'));
                            $this->data['conteudo'] = 'dicas/admin_edita';
                            $this->load->view('start/template', $this->data);
                    }
                    else
                    {
                        $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
                        $upload_data = $this->upload->data();
                        $file_uploaded = $upload_data['full_path'];
                        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];

                        if($this->image_moo->load($file_uploaded)->resize(690,690)->save($new_file,true))
                        {
                            $post['imagem'] = $upload_data['file_name'];
                        }
                        else
                        {
                            $post['imagem'] = NULL;
                        }
                    }
                }
                if($this->dica->change($post))
                {
                    $this->session->set_flashdata('success', 'Registro alterado 
                        com sucesso.');
                    redirect('painel/dicas');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Erro ao atualizar
                        registro. Tente novamente ou entre em contato com o 
                        suporte');
                    redirect('painel/dicas/editar/' . $post['id']);
                }
            }
        }
    }

    public function remove_foto($dica_id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Concept - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if($this->dica->remove_foto($dica_id))
            {
                $this->session->set_flashdata('success', 'Imagem removida com
                    sucesso.');
                redirect('painel/dicas/editar/' . $dica_id);
            }
            else
            {
                $this->session->set_flashdata('error', 'Erro ao atualizar
                    registro. Tente novamente ou entre em contato com o 
                    suporte');
                redirect('painel/dicas/editar/' . $dica_id);
            }
        }
    }

    public function processa_cadastro()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Concept - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('dicas'))
            {
                $this->data['acao'] = 'editar';
                $this->data['dica'] = $this->dica
                ->get_conteudo($this->input->post('id'));
                $this->data['conteudo'] = 'dicas/admin_edita';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }

                if(strlen($_FILES["imagem"]["name"])>0)
                {
                    
                    $config['upload_path'] = './assets/img/dicas/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '8000';
                    $config['max_width']  = '4000';
                    $config['max_height']  = '4000';

                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload('imagem'))
                    {
                            $this->data['error'] = array('error' => $this->upload->display_errors());
                            $this->data['acao'] = 'cadastrar';
                            $this->data['pagina'] = $this->pagina->get_conteudo($id, $this->input->post('id'));
                            $this->data['conteudo'] = 'dicas/admin_edita';
                            $this->load->view('start/template', $this->data);
                    }
                    else
                    {
                        $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
                        $upload_data = $this->upload->data();
                        $file_uploaded = $upload_data['full_path'];
                        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];

                        if($this->image_moo->load($file_uploaded)->resize(690,690)->save($new_file,true))
                        {
                            $post['imagem'] = $upload_data['file_name'];
                        }
                        else
                        {
                            $post['imagem'] = NULL;
                        }
                    }
                }

                if($this->dica->insert($post))
                {
                    $this->session->set_flashdata('success', 'Registro adicionado 
                        com sucesso.');
                    redirect('painel/dicas');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Erro ao adicionar
                        registro. Tente novamente ou entre em contato com o 
                        suporte');
                    redirect('painel/dicas/editar/' . $post['id']);
                }
            }
        }
    }

    public function deleta_dica($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Concept - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $apaga = $this->dica->apaga($id);
            if($apaga)
            {
                $this->session->set_flashdata('success', 'Registro removido com sucesso');
                redirect('painel/dicas');
            }
            else
            {
                $this->session->set_flashdata('error', 'Não foi possível remover o registro.
                    Tente novamente ou entre em contato com o suporte');
                redirect('painel/dicas/');
            }
        }
    }
}