<div class="row-fluid">
    <div class="span9">
        <legend><?=( $acao == 'editar' ) ? 'Editar' : 'Cadastrar'; ?> Dica</legend>
    <?php 
            switch ($acao) {
                case 'editar':
                    $action = 'dicas/admin_dicas/processa';
                    break;
                
                default:
                    $action = 'dicas/admin_dicas/processa_cadastro';
                    break;
            }
    ?>
    <?=form_open_multipart($action); ?>
    <?php if($acao == 'editar'): ?>
        <?=form_hidden('id', $dica->id); ?>
    <?php endif; ?>
    <?=form_label('Título (navegação)'); ?>
    <?=form_input(array(
        'name' => 'titulo_nav',
        'value' => set_value('titulo_nav', ($acao == 'editar') ? $dica->titulo_nav : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('titulo_nav'); ?>

    <?=form_label('Título (conteúdo)'); ?>
    <?=form_input(array(
        'name' => 'titulo_conteudo',
        'value' => set_value('titulo_conteudo', ($acao == 'editar') ? $dica->titulo_conteudo : ''),
        'class' => 'span8'
    )); ?>
    <?=form_error('titulo_conteudo'); ?>

    <?=form_label('Texto'); ?>
    <?=form_textarea(array(
        'name' => 'texto',
        'value' => set_value('texto', ($acao == 'editar') ? $dica->texto : ''),
        'class' => 'tinymce span9'
    )); ?>
    <?=form_error('texto'); ?>
    <div class="clearfix"></div>
    <br>
    <?php if($acao == 'editar' && $dica->imagem):?>
     <img src="<?php echo base_url(); ?>assets/img/dicas/<?php echo $dica->imagem; ?>" alt="" >
    <div class="clearfix"></div>
    <br>
     <a class="btn btn-danger btn-mini" href="<?php echo site_url('dicas/admin_dicas/remove_foto/' . $dica->id) ?>">Remover imagem</a>
     <?php endif; ?>
     <div class="control-group">
            <label class="control-label" for="imagem">Alterar Imagem</label>
            <div class="controls">
              <?php echo form_upload('imagem', set_value('imagem')); ?>
              <span class="help-inline"><?php echo form_error('imagem'); ?></span>
            </div>
     </div>
     <?=form_label('Vídeo'); ?>
    <?=form_textarea(array(
        'name' => 'video',
        'value' => set_value('video', ($acao == 'editar') ? $dica->video : ''),
        'class' => 'span9'
    )); ?>
    <?=form_error('video'); ?>
    <div class="clearfix"></div>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('dicas/admin_dicas/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
    </div>
</div>