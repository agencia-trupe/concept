<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Detalhes SEO
|
| Configurações básicas de SEO.
|--------------------------------------------------------------------------
*/
$config['site_name'] = 'Concept Arquitetura de Interiores';

$config['site_decription'] = 'Criação de Projetos e Reformas Residencial e Comercial (Corporativo).';

$config['site_keywords'] = 'Concept, Arquitetura, Interiores';

$config['site_author'] = 'Trupe Agência Criativa - http://trupe.net';
/* End of file seo.php */
/* Location: ./application/config/seo.php */