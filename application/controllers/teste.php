<?php
class Teste extends MX_Controller
{
    public function index()
    {
        $data['pagina'] = 'teste';
        $seo = array(
            'title' => 'Teste',
            'description' => 'Teste'
            );
        $this->load->library('seo', $seo);
        $data['conteudo'] = 'teste';
        $this->load->view('layout/template', $data);
    }
}