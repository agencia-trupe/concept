-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 09/08/2013 às 13h18min
-- Versão do Servidor: 5.5.32
-- Versão do PHP: 5.4.15-1~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `concept`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos`
--

CREATE TABLE IF NOT EXISTS `contatos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `endereco` varchar(400) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `uf` varchar(2) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telefone` varchar(12) NOT NULL,
  `cep` varchar(12) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `contatos`
--

INSERT INTO `contatos` (`id`, `endereco`, `bairro`, `cidade`, `uf`, `email`, `telefone`, `cep`, `twitter`, `facebook`) VALUES
(1, 'Rua Marques de Olinda, 316, Casa 05', 'Alterado', 'São Paulo', 'SP', 'atendimento@concept.arq.br', '11 7828 5696', '04277-000', 'teste2', 'http://fb.com/teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `dicas`
--

CREATE TABLE IF NOT EXISTS `dicas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo_nav` varchar(255) DEFAULT NULL,
  `titulo_conteudo` varchar(255) DEFAULT NULL,
  `texto` text,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `dicas`
--

INSERT INTO `dicas` (`id`, `titulo_nav`, `titulo_conteudo`, `texto`, `created`, `updated`) VALUES
(1, 'Por que contratar um profissional', 'Para o desenvolvimento de um bom projeto, sempre é bom escolher profissionais capacitados', '<p>Dica Concept Arquitetura de Interiores</p>', 1363372108, 0),
(2, 'Dica 11', 'Dica 11', '<p>Dica Concept Arquitetura de Interiores3wdqwdxczadc</p>', 1363178293, 0),
(3, 'Dica 2', 'Dica 2', '<p>Dica Concept Arquitetura de Interiores', 1362678872, NULL),
(4, 'Teste cadastro', 'Teste de cadastro de dica', '<p>Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem Ipsum Lorem Ipsum Lorem Ipsum&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>Lorem ipsum dolor</p>', 1363782263, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `fotos`
--

CREATE TABLE IF NOT EXISTS `fotos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projeto_id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) NOT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100 ;

--
-- Extraindo dados da tabela `fotos`
--

INSERT INTO `fotos` (`id`, `projeto_id`, `titulo`, `imagem`, `ordem`, `created`, `updated`) VALUES
(1, 0, 'Foto 0', 'foto_exemplo2.jpg', 3, 1362676238, 0),
(2, 0, 'Foto 1', 'foto_exemplo3.jpg', 7, 1362676238, 0),
(3, 0, 'Foto 2', 'foto_exemplo.jpg', 8, 1362676238, 0),
(4, 0, 'Foto 3', 'foto_exemplo.jpg', 4, 1362676238, 0),
(5, 0, 'Foto 4', 'foto_exemplo.jpg', 0, 1362676238, 0),
(6, 0, 'Foto 5', 'foto_exemplo.jpg', 5, 1362676238, 0),
(7, 0, 'Foto 6', 'foto_exemplo.jpg', 6, 1362676238, 0),
(8, 0, 'Foto 7', 'foto_exemplo4.jpg', 1, 1362676238, 0),
(31, 0, 'Teste', 'imagem-atuacao.jpg', 0, 1363280550, NULL),
(32, 0, 'Teste', '112.jpg', 0, 1363280606, NULL),
(33, 0, 'Teste', '113.jpg', 0, 1363280650, NULL),
(35, 0, 'Teste', '4.jpg', 0, 1363281524, NULL),
(36, 0, 'Teste', '41.jpg', 0, 1363281585, NULL),
(37, 0, 'Teste', '3.jpg', 0, 1363281829, NULL),
(62, 0, 'Teste', '48.jpg', 0, 1363289111, 0),
(77, 0, 'Teste', '640x9601.jpg', 2, 1363291729, 0),
(78, 0, 'Teste', '480x8004.jpg', 1, 1363360280, 0),
(79, 0, 'Teste', '1280x800.jpg', 0, 1363360378, 0),
(80, 0, 'Teste', '1920x1080.jpg', 0, 1363364415, NULL),
(88, 0, 'Teste', '1152189297_13e08144b7_z.jpg', 0, 1363703226, NULL),
(89, 0, 'Teste', '2719386997_d59889f882.jpg', 0, 1363703230, NULL),
(90, 0, 'Teste', '1152189297_13e08144b7_z1.jpg', 3, 1363703271, 0),
(91, 0, 'Teste', '2719386997_d59889f8821.jpg', 4, 1363703278, 0),
(92, 0, 'Teste', 'Modern_decoration_at_Larnaca_International_Airport_Republic_of_Cyprus.JPG', 2, 1363703281, 0),
(93, 0, 'Teste', '2719386997_d59889f8822.jpg', 1, 1363780250, 0),
(94, 0, 'Teste', '1152189297_13e08144b7_z2.jpg', 0, 1363780254, 0),
(95, 0, 'Teste', '2719386997_d59889f8823.jpg', 0, 1363782041, 0),
(96, 0, 'Teste', '1152189297_13e08144b7_z3.jpg', 1, 1363782127, 0),
(97, 7, 'Teste', '38.jpg', 0, 1364495839, 0),
(98, 7, 'Teste', '39.jpg', 1, 1364495954, 0),
(99, 7, 'Teste', 'projeto_2.jpg', 0, 1365616407, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `midia`
--

CREATE TABLE IF NOT EXISTS `midia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) NOT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Extraindo dados da tabela `midia`
--

INSERT INTO `midia` (`id`, `titulo`, `imagem`, `ordem`, `created`, `updated`) VALUES
(4, 'Teste Mídia', 'imagem-atuacao.jpg', 0, 1363703745, 1363703745),
(5, 'Teste Mídia 2', 'imagem-atuacao1.jpg', 0, 1363703755, 0),
(7, 'Teste', 'imagem-atuacao2.jpg', 0, 1363704128, 1363704128),
(8, 'Teste Cadastro', '2719386997_d59889f882.jpg', 0, 1363782206, 1363782206);

-- --------------------------------------------------------

--
-- Estrutura da tabela `paginas`
--

CREATE TABLE IF NOT EXISTS `paginas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `texto` text,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `template` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `paginas`
--

INSERT INTO `paginas` (`id`, `titulo`, `texto`, `imagem`, `ativo`, `created`, `updated`, `slug`, `description`, `template`) VALUES
(1, 'Pefil Teste de Alteração', '<p>P&aacute;gina de perfil teste de<span style="text-decoration: line-through;"><strong> altera&ccedil;&atilde;o de texto</strong></span></p>', 'imagem-perfil2.jpg', 1, 1362513475, 0, 'perfil', '<p>Perfil Concept Arquitetura</p>', ''),
(3, 'Atuação', '<p><strong>Atua&ccedil;&atilde;o&nbsp;</strong></p>\n<p>Lorem ipsum dolor</p>', 'imagem-atuacao3.jpg', 1, 1362659707, 0, 'atuacao', '', 'atuacao');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos`
--

CREATE TABLE IF NOT EXISTS `projetos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `texto` text,
  `capa` varchar(255) DEFAULT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Extraindo dados da tabela `projetos`
--

INSERT INTO `projetos` (`id`, `titulo`, `texto`, `capa`, `created`, `updated`, `ordem`) VALUES
(7, 'Projeto 1', '<p>Projeto teste</p>', 'projeto_21.jpg', 1364471318, 0, 1),
(8, 'Projeto 2', '<p>Projeto teste</p>', 'projeto_21.jpg', 1365507454, 0, 2),
(9, 'Projeto 3', '<p>Projeto teste</p>', 'projeto_21.jpg', 1365507454, NULL, 3),
(10, 'Projeto 4', '<p>Projeto teste</p>', 'projeto_21.jpg', 1364471318, NULL, 4),
(11, 'Projeto 5', '<p>Projeto teste</p>', 'projeto_21.jpg', 1365507454, NULL, 5),
(12, 'Projeto 6', '<p>Projeto teste</p>', 'projeto_21.jpg', 1365507454, NULL, 6),
(13, 'Projeto 7', '<p>Projeto teste</p>', 'projeto_21.jpg', 1365507454, NULL, 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) NOT NULL,
  `default` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `roles`
--

INSERT INTO `roles` (`id`, `role`, `default`) VALUES
(1, 'admin', 0),
(2, 'user', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE IF NOT EXISTS `servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` text,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id`, `titulo`, `descricao`, `created`, `updated`) VALUES
(2, 'Serviço teste 2', '<ul>\n<li>Conte&uacute;do 0</li>\n<li>Conte&uacute;do 1</li>\n<li>Conte&uacute;do 2</li>\n</ul>', 1362661426, 0),
(3, 'Serviço teste 1', '<ul>\n<li>Conte&uacute;do 0</li>\n<li>Conte&uacute;do 1</li>\n<li>Conte&uacute;do 2</li>\n</ul>', 1362661434, 0),
(4, 'Serviço Teste 3', '<ul>\n<li>Item 0</li>\n<li>Item 1</li>\n<li>Item 2</li>\n</ul>', 1363782196, 1363782196);

-- --------------------------------------------------------

--
-- Estrutura da tabela `slides`
--

CREATE TABLE IF NOT EXISTS `slides` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `padrao` tinyint(1) NOT NULL,
  `ordem` int(11) NOT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `slides`
--

INSERT INTO `slides` (`id`, `titulo`, `link`, `imagem`, `padrao`, `ordem`, `created`, `updated`) VALUES
(6, 'Slide teste 1', 'http://link1.com', 'home_slide2.jpg', 0, 2, 0, 0),
(9, 'Teste 2', 'http://trupe.net', '1.jpg', 0, 1, 0, NULL),
(10, 'Teste', 'http://teste.com', '1152189297_13e08144b7_z.jpg', 0, 3, 0, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `role_id` int(11) NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `role_id`, `activated`, `banned`, `ban_reason`, `new_password_key`, `new_password_requested`, `new_email`, `new_email_key`, `last_ip`, `last_login`, `created`, `modified`) VALUES
(1, 'trupe', '$P$Bm.afp74faAjfqPGOJPYzFREhWoN/S/', 'nilton@trupe.net', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '127.0.0.1', '2013-06-13 12:07:56', '2013-03-06 15:59:08', '2013-06-13 12:07:56'),
(2, 'concept', '$P$BhRioMVL4QVizUtGRFqO8vwbDzE6FE1', 'niltondefreitas@gmail.com', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '127.0.0.1', '2013-03-20 16:53:14', '2013-03-20 16:52:29', '2013-03-20 19:53:14');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_autologin`
--

CREATE TABLE IF NOT EXISTS `user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_profiles`
--

CREATE TABLE IF NOT EXISTS `user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `country`, `website`) VALUES
(1, 1, NULL, NULL),
(2, 2, NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
